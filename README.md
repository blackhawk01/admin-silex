# admin-silex
silex1.3 template with adminlte

### Technology Used
- silex 1.3
- Twig templating
- Yaml File Parsing configuration

### Architecture
- Admin
    - Cms part of the website CRUD
    - Can be accessed using /admin
- API
    - Rest Api of the website dealings
    - Can be accessed using /api
- Front
    - Actual Web Application
    - Can be accessed using /

### Application Flow
#### Authentification
- Application Auth -> ROLE_APP
    - Makes Request on behalf of Applications(Mobile & Web App).
- User Auth -> ROLE_USER
    - Makes Request on behalf of the user.
- Driver Auth -> ROLE_DRIVER
    - Makes request on behalf of the driver.
```
    Request:
    POST
    {{host}}/api/login

    {
    	"_username" : "xxx",
    	"_password"	: "xxx"
    }

    Response:
    {
      "success": true,
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiYWRtaW4iLCJleHAiOjE0ODUzNDQ4MTZ9.lNbvuYH_GFkSqnA8EgfkL0r_UBcszWWv5SXkjvdegLw"
    }
```

- - Before any other request while application start authenticate the application , then the user save the tokens seperately to make request on behalf of the required role.
#### User
- Username Validation
    - access: ROLE_APP