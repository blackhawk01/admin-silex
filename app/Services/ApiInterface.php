<?php
/**
 * Created by PhpStorm.
 * User: esokia
 * Date: 20/01/17
 * Time: 12:28
 */

namespace AKCMS\AKApi;


interface ApiInterface
{
    function getOne($id);
    function getAll();
    function save($data);
    function update($id, $data);
    function delete($id);
}