<?php
/**
 * Created by PhpStorm.
 * User: esokia
 * Date: 20/01/17
 * Time: 12:32
 */

namespace AKCMS\AKApi;
require 'ApiService.php';
require 'ApiInterface.php';


class ApiModelService extends ApiService implements ApiInterface
{

    public function getOne($id)
    {
        return $this->db->fetchAssoc("SELECT * FROM $this->model WHERE id=?", [(int) $id]);
    }
    public function getAll()
    {
        return $this->db->fetchAll("SELECT * FROM $this->model");
    }
    function save($data)
    {
        $this->db->insert($this->model, $data);
        return $this->db->lastInsertId();
    }
    function update($id, $data)
    {
        return $this->db->update($this->model, $data, ['id' => $id]);
    }
    function delete($id)
    {
        return $this->db->delete($this->model, array("id" => $id));
    }
}