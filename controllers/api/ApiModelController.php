<?php
/**
 * Created by PhpStorm.
 * User: esokia
 * Date: 20/01/17
 * Time: 14:00
 */

namespace AKCMS\AKApi;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiModelController
{
    protected $service;

    public function __construct(ApiModelService $service)
    {
        $this->service = $service;
    }

    public function getOne($id)
    {
        $result = $this->service->getOne($id);
        if($result){
            return new JsonResponse($result);
        }else{
            return new JsonResponse(null,404);
        }

    }
    public function getAll()
    {
        return new JsonResponse($this->service->getAll());
    }
    public function save(Request $request)
    {
        return new JsonResponse(array("id" => $this->service->save(json_decode($request->getContent(), true)),201));
    }
    public function update($id, Request $request)
    {
        $data = $this->getDataFromRequest($request);
        $this->service->update($id, $data);
        return new JsonResponse($data);
    }
    public function delete($id)
    {
        return new JsonResponse($this->service->delete($id));
    }
    public function getDataFromRequest(Request $request)
    {
        return $data = array(
            "data" => $request->request->get("data")
        );
    }
}