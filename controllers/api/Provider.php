<?php

/**
 * Created by PhpStorm.
 * User: terence
 * Date: 17/01/2017
 * Time: 08:08
 */
namespace AKCMS\AKApi;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
require 'Controller.php';
require 'ApiModelController.php';
require 'app/Services/ApiModelService.php';

use AKCMS\AKApi\Controller;

class Provider implements ControllerProviderInterface
{

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {


        $controllers = $app['controllers_factory'];
        $app['section'] = '/api';

        $controllers->post('/login','AKCMS\AKApi\Controller::login')->bind('api.login');
        $controllers->get('/hello','AKCMS\AKApi\Controller::hello')->bind('api.hello');




        $this->bindRoutesToControllers('users',$app,$controllers);
        $this->bindRoutesToControllers('test',$app,$controllers);

        return $controllers;
    }

    public function bindRoutesToControllers($model,$app,$controllers)
    {
        $app["$model.service"] = function() use($app,$model) {
            return new ApiModelService($app["db"],$model);
        };

        $app["api.$model"] = function() use($app,$model) {
            return new ApiModelController($app["$model.service"]);
        };
        $controllers->get("/$model", "api.$model:getAll");
        $controllers->get("/$model/{id}", "api.$model:getOne");
        $controllers->post("/$model", "api.$model:save");
        $controllers->put("/$model/{id}", "api.$model:update");
        $controllers->delete("/$model/{id}", "api.$model:delete");
        $app->mount('/', $controllers);
    }
}